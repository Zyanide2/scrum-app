<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class AccountsController extends Controller
{
    protected $redirectTo = "/login";

    public function __construct()
    {
        $this->middleware("auth");
        $this->middleware("admin");
    }

    public function showAccountCollectionView(Request $request)
    {
        $usersPerPage = 20;
        $authority = $request->authority=="%"||$request->authority==""?["authority","LIKE","%"]:["authority","=",$request->authority];
        $users = User::query()->where([["email","LIKE","%$request->email%"],$authority])->skip($request->has("page")?(($request->page-1)*$usersPerPage):0)->take($usersPerPage)->get();

        $count = User::query()->where([["email","LIKE","%$request->email%"],$authority])->count();
        $count = $count%$usersPerPage==0?$count/$usersPerPage:ceil(($count/$usersPerPage)+0.1);

        return view("Admin.AccountCollection", ["users"=>$users, "lastPageNo"=>$count]);
    }

    public function showAccountSingleView(Request $request)
    {
        if(!$request->has("id"))
        {
            return redirect("admin/accounts");
        }

        $user = User::query()->where("id", "=", $request->id)->first();

        if($user == null)
        {
            return redirect("admin/accounts");
        }

        return view("Admin.AccountSingle", ["user"=>$user]);
    }

    public function showAccountEditView(Request $request, $detail)
    {
        return "AccountEditView | $detail | $request->id";
    }
}
