<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
    }

    public function showAccountView()
    {
        return view("Auth.Account");
    }

    public function showEditView($detail)
    {
        $detailTitles = [
            "pfp"=>"Profile picture",
            "name"=>"Name",
            "birth"=>"Date of birth",
            "gender"=>"Gender",
            "pwd"=>"Password",
            "email"=>"E-Mail",
            "tel"=>"Telephone",
        ];
        return view("Auth.EditAccount", ["detail"=>$detail, "detailTitle"=>$detailTitles[$detail]]);
    }

    public function editAccount(Request $request)
    {
        if(!$request->hasAny(["pfp", "firstName", "lastName", "password", "password_confirmation", "email"]))
        {
            return view("Auth.Account")->withErrors(["empty"=>"empty"]);
        }

        $validator = Validator::make($request->only(["pfp","firstName","lastName","password","password_confirmation","email"]), [
            "pfp"=>"file|image|max:50",
            "firstName"=>"min:2|max:64",
            "lastName"=>"min:2|max:64",
            "password"=>"required_with:password_confirmation|min:6|max:128",
            "password_confirmation"=>"required_with:password|same:password",
            "email"=>"email|max:255|unique:users,email",
        ],[
            "file"=>"This field must be a file.",
            "image"=>"The uploaded file must be an image.",
            "pfp.max"=>"The uploaded file must be 50kb or less in size.",
            "required_with"=>"You're required to fill in the 'New password' and the 'Repeat new password' field when resetting your password.",
            "same"=>"The 'New password' and the 'Repeat new password' are not equal.",
            "email"=>"You must submit a valid E-Mail.",
            "unique"=>"The submitted E-Mail already exists."
        ]);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator);
        }

        $user = \Auth::user();

        if($request->has("pfp"))
        {
            $newFileName = \Auth::id().".".$request->file("pfp")->extension();
            if(File::exists("public/img/uploads/pfp/".$newFileName))
            {
                File::delete("public/img/uploads/pfp/".$newFileName);
            }
            $request->file("pfp")->storeAs("pfp", $newFileName, "upload");
            $user->pfp = $newFileName;
        }
        if($request->has("firstName"))
        {
            $user->firstName = $request->input("firstName");
        }
        if($request->has("lastName"))
        {
            $user->lastName = $request->input("lastName");
        }
        if($request->has("email"))
        {
            $user->email = $request->input("email");
            $user->email_verified_at = null;
            // TODO: send verification email
        }
        if($request->has("password") && $request->has("password_confirmation"))
        {
            $user->password = Hash::make($request->input("password"));
        }

        if(!$user->save())
        {
            return abort(500,"Could not save the new information into the database");
        }

        return redirect("account")->withErrors(["success"=>"The changes has been successfully applied."]);
    }
}
