<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Project;
use App\UserProject;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    protected $redirectTo = "/login";

    public function __construct()
    {
        $this->middleware("auth");
    }

    public function showProjectCollectionView(Request $request)
    {
        $projectsPerPage = 4;
        if(\Auth::user()->isAuthorized("administrator"))
        {
            $projects = Project::query()->where("projects.title", "LIKE", "%{$request->title}%")
                ->skip($request->has("page")?($request->page-1)*$projectsPerPage:0)->take($projectsPerPage)->get();

            $count = Project::query()->where("title", "LIKE", "%{$request->title}%")->select("projects.*")->count();
        }
        else
        {
            $projects = Project::query()->where([["projects.title", "LIKE", "%{$request->title}%"],["user_projects.userId", "=", \Auth::id()]])
                ->join("user_projects", "user_projects.projectId", "=", "projects.id")->select("projects.*")
                ->skip($request->has("page")?($request->page-1)*$projectsPerPage:0)->take($projectsPerPage)->get();

            $count = Project::query()->where([["projects.title", "LIKE", "%{$request->title}%"],["user_projects.userId", "=", \Auth::id()]])
                ->join("user_projects", "user_projects.projectId", "=", "projects.id")->select("projects.*")->count();
        }

        $count = $count%$projectsPerPage==0?($count/$projectsPerPage):ceil(($count/$projectsPerPage)+0.1);

        return view("App.ProjectCollection", ["projects"=>$projects, "lastPageNo"=>$count]);
    }

    public function showProjectSingleView($uid)
    {
        if(!\Auth::user()->isAuthorized("administrator"))
        {
            if(UserProject::query()->where([["projectId","=",$uid],["userId","=",\Auth::id()]])->count() == 0)
            {
                return abort(403, "You are not authorized to visit the requested page");
            }
        }
        else
        {
            if(Project::query()->where("id","=",$uid)->count() == 0)
            {
                return abort(404, "Could not find the requested page!");
            }
        }

        return view("App.ProjectSingle", ["uid"=>$uid]);
    }
}
