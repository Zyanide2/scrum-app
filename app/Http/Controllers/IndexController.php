<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function show()
    {
        if(\Auth::check())
        {
            return redirect("dashboard");
        }
        return view("Home");
    }
}
