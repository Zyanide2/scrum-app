<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    const ACCOUNT_TYPES = [
        "administrator",
        "scrumMaster",
        "productOwner",
        "developer",
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'pfp', 'firstName', 'lastName', 'authority'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAuthorized($authorityNeeded)
    {
        if(!in_array($authorityNeeded, User::ACCOUNT_TYPES))
        {
            return abort(501, "The authority: '$authorityNeeded' does not exist in the class 'User' in the 'ACCOUNT_TYPES' variable.");
        }
        return in_array($this->authority, explode(",", $authorityNeeded) ?? $authorityNeeded) ?? false;
    }
}
