@extends ("layouts.App")
@section ("head")
    <link rel="stylesheet" type="text/css" href="{{ asset("css/EditAccount.css") }}">
@endsection
@section ("title")
    Scrum-app
@endsection
@section ("main")
    @if ($errors->any())
        <p class="alert alert-danger">{{ $errors->first() }}</p>
    @endif
    <div class="w-100 pt-4">
        <div class="text-center">
            <a href="{{ url("account") }}"><i class="fas fa-arrow-left fa-lg"></i></a>
            <span class="ml-4" style="font-size: 2em;">{{ $detailTitle }}</span>
            <hr>
        </div>
    </div>
    <div class="d-flex justify-content-center">
        <div class="card">
            <div class="card-body">
                @if ($detail === "pfp")
                    <img class="rounded-circle" width="250px" height="250px" src="{{ asset("img/uploads/pfp/".(\Auth::user()->pfp==null?"default.png":\Auth::user()->pfp)) }}" alt="profile picture">
                @elseif ($detail === "name")
                    {{ \Auth::user()->firstName." ".\Auth::user()->lastName }}
                @elseif ($detail === "pwd")
                    <small>
                        <i class="fas fa-circle fa-xs"></i>
                        <i class="fas fa-circle fa-xs"></i>
                        <i class="fas fa-circle fa-xs"></i>
                        <i class="fas fa-circle fa-xs"></i>
                        <i class="fas fa-circle fa-xs"></i>
                        <i class="fas fa-circle fa-xs"></i>
                    </small>
                @elseif ($detail === "email")
                    {{ \Auth::user()->email }}
                @endif
                <span class="float-right mt-1" onclick="showModal();"><i class="fas fa-pen"></i></span>
            </div>
        </div>
    </div>
    <modal>
        <form action="{{ route("account.edit") }}" method="post" enctype="multipart/form-data" class="card sharp-corner pb-2 pt-4 px-3">
            @csrf
            <div class="card-body p-0">
                <div class="card-title">
                    <span class="float-left h5">Edit {{ $detailTitle }}</span>
                    <span class="float-right mr-2 mt-1" onclick="closeModal();"><i class="fas fa-times fa-lg"></i></span>
                </div>
                <br><br>
                @if ($detail === "pfp")
                    <img class="rounded-circle" width="250px" height="250px" src="{{ asset("img/uploads/pfp/".(\Auth::user()->pfp==null?"default.png":\Auth::user()->pfp)) }}" alt="Profile picture">
                    <br><br>
                    <input class="form-control-file" accept="image/*" type="file" name="pfp">
                @elseif ($detail === "name")
                    <small class="text-secondary">First name</small>
                    <div class="separator"><input class="form-control sharp-corner border-top-0 border-left-0 border-right-0" type="text" name="firstName" value="{{ \Auth::user()->firstName }}"></div>
                    <small class="text-secondary">Last name</small>
                    <div class="separator"><input class="form-control sharp-corner border-top-0 border-left-0 border-right-0" type="text" name="lastName" value="{{ \Auth::user()->lastName }}"></div>
                @elseif ($detail === "pwd")
                    <small class="text-secondary">New password</small>
                    <div class="separator"><input class="form-control sharp-corner border-top-0 border-left-0 border-right-0" type="password" name="password"></div>
                    <small class="text-secondary">Repeat new password</small>
                    <div class="separator"><input class="form-control sharp-corner border-top-0 border-left-0 border-right-0" type="password" name="password_confirmation"></div>
                @elseif ($detail === "email")
                    <small class="text-secondary">E-Mail</small>
                    <div class="separator"><input class="form-control sharp-corner border-top-0 border-left-0 border-right-0" type="email" name="email" value="{{ \Auth::user()->email }}"></div>
                @endif
                    <br>
                <button type='submit' class="btn bg-transparent float-right text-primary">DONE</button>
                <button type='button' class="btn bg-transparent float-right ml-4" onclick="closeModal();">CANCEL</button>
            </div>
        </form>
    </modal>
@endsection
@section ("scripts")
    <script>
        function showModal()
        {
            $("modal").css({visibility: "visible"});
            @if ($detail === "pfp")
                $("img[alt='Profile picture']").attr("src", "{{ asset("img/uploads/pfp/".(\Auth::user()->pfp==null?"default.png":\Auth::user()->pfp)) }}");
            @elseif ($detail === "name")
                $("input[name=firstName]").val("{{ \Auth::user()->firstName }}");
                $("input[name=lastName]").val("{{ \Auth::user()->lastName }}");
            @elseif ($detail === "email")
                $("input[name=email]").val("{{ \Auth::user()->email }}");
            @elseif ($detail === "pwd")
                $("input[name=password]").val("");
                $("input[name=password_confirmation]").val("");
            @endif
        }

        function closeModal()
        {
            $("modal").css({visibility: "hidden"});
        }

        @if ($detail === "pfp")
            $("input[type=file]").change(()=>{
                let input = $("input[type=file]")[0];
                if (input.files && input.files[0]) {
                    let reader = new FileReader();

                    reader.onload = function(e) {
                        $("img[alt='Profile picture']").attr("src", e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            });
        @endif
    </script>
@endsection
