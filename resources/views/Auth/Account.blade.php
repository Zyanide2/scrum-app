@extends ("layouts.App")
@section ("head")
    <link rel="stylesheet" type="text/css" href="{{ asset("css/Account.css") }}">
@endsection
@section ("title")
    Scrum-app
@endsection
@section ("main")
    @if ($errors->has("success"))
        <p class="alert alert-success">{{ $errors->first("success") }}</p>
    @endif
    <div class="w-100 text-center py-4"><h1>Account info</h1></div>
    <div>
        <div class="card">
            <div class="card-header">
                Profile
            </div>
            <ul class="list-group list-group-flush">
                <a href="{{ url("account/pfp") }}" class="list-group-item list-group-item-action"><div class="list-group-item-assign">Profile picture</div><img class="rounded-circle" width="60px" height="60px" src="{{ asset("img/uploads/pfp/".(\Auth::user()->pfp==null?"default.png":\Auth::user()->pfp)) }}" alt="Profile picture"><i class="fas fa-chevron-right mt-4"></i></a>
                <a href="{{ url("account/name") }}" class="list-group-item list-group-item-action"><div class="list-group-item-assign">Name</div>{{ \Auth::user()->firstName." ".\Auth::user()->lastName }}<i class="fas fa-chevron-right"></i></a>
                <a href="{{ url("account/pwd") }}" class="list-group-item list-group-item-action"><div class="list-group-item-assign">Password</div>
                    <small><small><small><i class="fas fa-circle"></i> <i class="fas fa-circle"></i> <i class="fas fa-circle"></i> <i class="fas fa-circle"></i> <i class="fas fa-circle"></i> <i class="fas fa-circle"></i></small></small></small>
                <i class="fas fa-chevron-right"></i></a>
            </ul>
        </div>
        <div class="card">
            <div class="card-header">
                Contact info
            </div>
            <ul class="list-group list-group-flush">
                <a href="{{ url("account/email") }}" class="list-group-item list-group-item-action"><div class="list-group-item-assign">E-Mail</div>{{ \Auth::user()->email }}<i class="fas fa-chevron-right"></i></a>
            </ul>
        </div>
    </div>
@endsection
