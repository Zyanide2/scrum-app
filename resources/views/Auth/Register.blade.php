@extends ("layouts.App")
@section ("head")
    <link rel="stylesheet" type="text/css" href="{{ asset("css/Register.css") }}">
@endsection
@section ("title")
    Scrum-app
@endsection
@section ("main")
    {{ isset($_SESSION["alert"])?$_SESSION["alert"]->show():"" }}
<div>
    <span class="h1">REGISTER</span>
    <form action="{{ route("register") }}" method="post" autocomplete="off">
        @csrf
        First name:
        <div class="separator"><input class="form-control" type="text" name="firstName" placeholder="First name"></div>
        Last name:
        <div class="separator"><input class="form-control" type="text" name="lastName" placeholder="Last name"></div>
        E-mail:
        <div class="separator"><input class="form-control" type="email" name="email" placeholder="E-mail"></div>
        Password:
        <div class="separator"><input class="form-control" type="password" name="password" placeholder="Password"></div>
        Repeat password:
        <div class="separator"><input class="form-control" type="password" name="password_confirmation" placeholder="Repeat password"></div>
        Authority:
        <div class="separator">
            <select class="form-control" name="authority">
                @foreach (App\User::ACCOUNT_TYPES as $type)
                    @if ($type!="administrator")
                        <option value="{{ $type }}">{{ $type }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <button class="btn btn-primary sharp-corner px-3 py-1" type="submit">Register</button>
    </form>
</div>
@endsection
