@extends ("layouts.Guest")
@section ("head")
<link rel="stylesheet" type="text/css" href="{{ asset("css/Login.css") }}">
@endsection
@section ("title")
Scrum-app
@endsection
@section ("main")
{{ isset($_SESSION["alert"])?$_SESSION["alert"]->show():"" }}
<div>
    <span class="h1">LOGIN</span>
    <form action="{{ route("login") }}" method="post" autocomplete="off">
        @csrf
        E-mail:
        <div class="separator"><input class="form-control" type="email" name="email" placeholder="E-mail"></div>
        Password:
        <div class="separator"><input class="form-control" type="password" name="password" placeholder="Password"></div>
        <button class="btn btn-primary sharp-corner px-3 py-1" type="submit">Login</button>
    </form>
</div>
@endsection
