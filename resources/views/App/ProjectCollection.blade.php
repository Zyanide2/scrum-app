@extends ("layouts.App")
@section ("head")
    <link rel="stylesheet" type="text/css" href="{{ asset("css/Project.css") }}">
@endsection
@section ("title")
    Scrum-app
@endsection
@section ("main")
    <form method="get">
        <div id="searchBar" class="d-inline-block"><input class="form-control" type="text" name="title" placeholder="Search..." autocomplete="off"></div>
        <button type="submit" class="btn btn-primary sharp-corner d-inline ml-3">Filter</button>
    </form>
    <hr>
    <div>
        @foreach ($projects as $project)
            <div class="card" onclick="window.location.href = '{{ asset("projects/".$project->id) }}'">
                <div class="card-header">
                    {{ $project->title }}
                </div>
                <div class="card-body">
                    Deadline: <b>{{ $project->deadline }}</b><br>
                    {{ $project->definitionOfDone }}<br>
                    <a href="{{ asset("pdf/uploads/pod/".$project->projectDefinition) }}">{{ $project->projectDefinition }}</a>
                </div>
            </div>
        @endforeach
    </div>
    <div id="pagination">
        <?php
            function getPageLink($pageNo)
            {
                $search = "";
                foreach($_GET as $key=>$val)
                {
                    if($key != "page")
                    {
                        $search .= "&$key=$val";
                    }
                }
                return url("projects?page=".$pageNo.$search);
            }
        ?>
        @if ($lastPageNo <= 5)
            @for ($i = 1; $i <= $lastPageNo; $i++)
                <a style="display: inline-block; margin: 0 2px;" href="{{ getPageLink($i) }}">{{ $i }}</a>
            @endfor
        @else
            <a style="display: inline-block; margin: 0 2px;" href="{{ getPageLink(1) }}">1</a>{{ isset($_GET["page"])?($_GET["page"]-2>2?"...":""):"" }}
            @for ($i = 0; $i < 3; $i++)
                <?php $pageNo = min(($lastPageNo-(3-$i)), max(2+$i, (isset($_GET["page"])?$_GET["page"]:1)-(2-$i))); ?>
                <a style="display: inline-block; margin: 0 2px;" href="{{ getPageLink($pageNo) }}">{{ $pageNo }}</a>
            @endfor
            {{ isset($_GET["page"])?($_GET["page"]+4<$lastPageNo?"...":""):"" }}<a style="display: inline-block; margin: 0 2px;" href="{{ getPageLink($lastPageNo) }}">{{ $lastPageNo }}</a>
        @endif
    </div>
@endsection
