<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" type="text/css" href="{{ asset("/bootstrap/css/bootstrap-custom-4.3.1.min.css") }}">
        <link rel="stylesheet" type="text/css" href="{{ asset("/devicon/devicon.min.css") }}">
        <!--<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">-->
        <!--<script src="https://kit.fontawesome.com/b0fcc72b2f.js" crossorigin="anonymous"></script>-->
        <link rel="stylesheet" type="text/css" href="{{ asset("font-awesome/css/all.min.css") }}">
        <link rel="stylesheet" type="text/css" href="{{ asset("css/Default.css") }}">
        @yield ("head")
        <title>@yield ("title")</title>
    </head>
    <body>
        <header class="container-fluid px-0 fixed-top">
            <nav class="navbar navbar-expand-sm navbar-dark bg-primary mx-0">
                    <ul class="navbar-nav h-100">
                        <div class="navbar-brand">
                            <li class="nav-item h-100">
                                <a href="{{ url("home") }}" class="nav-link h-100">
                                    <img alt="Scrum-app logo" src="{{ asset("img/logo.png") }}">
                                </a>
                            </li>
                        </div>
                        <div class="d-sm-block d-none ml-5">
                            <li class="nav-item"><a class="nav-link" href="{{ url("home") }}">Home</a></li>
                        </div>
                    </ul>
                <div class="w-100 d-none d-sm-block">
                    <ul class="navbar-nav float-right">
                        <li class="nav-item"><a class="nav-link" href="{{ url("login") }}">Login</a></li>
                    </ul>
                </div>
                <div class="ml-auto navbar-expand d-block d-sm-none">
                    <ul class="navbar-nav float-right pr-2">
                        <li class="nav-item"><a href="{{ url("login") }}" class="nav-link">Login</a></li>
                        <li class="nav-item">
                            <i class="nav-link fas fa-bars fa-2x" onclick="$('#sidebar-collapsed').collapse('toggle');"></i>
                        </li>
                    </ul>
                </div>
                <div id="sidebar-collapsed" class="collapse navbar-collapse">
                    <ul class="navbar-nav text-center d-block d-sm-none">
                        <li class="nav-item"><a href="{{ url("login") }}" class="nav-link"><i class="fa fa-home fa-2x"></i> Home</a></li>
                    </ul>
                </div>
            </nav>
        </header>
        <div id="relative-content" class="ml-0 w-100">
            <main>
                @yield ("main")
            </main>
            <footer class="container-fluid w-100 px-0 h-auto bg-dark text-light p-2">
                Footer
            </footer>
        </div>
        <script rel="script" type="text/javascript" src="{{ asset("jquery/jquery-3.4.1.min.js") }}"></script>
        <script rel="script" type="text/javascript" src="{{ asset("popper/popper-1.14.7.min.js") }}"></script>
        <script rel="script" type="text/javascript" src="{{ asset("bootstrap/js/bootstrap-4.3.1.min.js") }}"></script>
        @yield ("scripts")
    </body>
</html>
