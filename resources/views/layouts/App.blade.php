<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" type="text/css" href="{{ asset("/bootstrap/css/bootstrap-custom-4.3.1.min.css") }}">
        <link rel="stylesheet" type="text/css" href="{{ asset("/devicon/devicon.min.css") }}">
        <!--<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">-->
        <!--<script src="https://kit.fontawesome.com/b0fcc72b2f.js" crossorigin="anonymous"></script>-->
        <link rel="stylesheet" type="text/css" href="{{ asset("font-awesome/css/all.min.css") }}">
        <link rel="stylesheet" type="text/css" href="{{ asset("css/Default.css") }}">
        @yield ("head")
        <title>@yield ("title")</title>
    </head>
    <body>
        <header class="container-fluid px-0 fixed-top">
            <nav class="navbar navbar-expand-sm navbar-dark bg-primary mx-0">
                <div class="navbar-brand">
                    <ul class="navbar-nav h-100">
                        <li class="nav-item h-100">
                            <a href="{{ url("home") }}" class="nav-link h-100">
                                <img alt="Scrum-app logo" src="{{ asset("img/logo.png") }}">
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="w-100 d-none d-sm-block">
                    <ul class="navbar-nav float-right">
                        <li class="text-light mt-3">
                            {{ (\Auth::user()->firstName." ".\Auth::user()->lastName) }}
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown">
                                @if (\Auth::user()->pfp==null)
                                    <img src="{{ asset("img/uploads/pfp/default.png") }}" alt="profile picture" width="40px" height="40px" class="rounded-circle">
                                @else
                                    <img src="{{ asset("img/uploads/pfp/".\Auth::user()->pfp) }}" alt="profile picture" width="40px" height="40px" class="rounded-circle">
                                @endif
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <form action="{{ route("logout") }}" method="post" class="m-0">
                                    @csrf
                                    <button class="dropdown-item" type="submit">Logout</button>
                                </form>
                                <a class="dropdown-item" href="{{ url("account") }}">Edit account</a>
                                @if (\Auth::user()->isAuthorized("administrator"))
                                    <a class="dropdown-item" href="{{ url("admin/accounts") }}">Manage accounts</a>
                                @endif
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="ml-auto navbar-expand d-block d-sm-none">
                    <ul class="navbar-nav float-right pr-2">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown">
                                @if (\Auth::user()->pfp==null)
                                    <img src="{{ asset("img/uploads/pfp/default.png") }}" alt="profile picture" width="30px" height="30px" class="rounded-circle">
                                @else
                                    <img src="{{ asset("img/uploads/pfp/".\Auth::user()->pfp) }}" alt="profile picture" width="30px" height="30px" class="rounded-circle">
                                @endif
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <form action="{{ route("logout") }}" method="post" class="m-0">
                                    @csrf
                                    <button class="dropdown-item" type="submit">Logout</button>
                                </form>
                                <a class="dropdown-item" href="{{ url("account") }}">Edit account</a>
                                @if (\Auth::user()->isAuthorized("administrator"))
                                    <a class="dropdown-item" href="{{ url("admin/accounts") }}">Manage accounts</a>
                                @endif
                            </div>
                        </li>
                        <li class="nav-item">
                            <i class="nav-link fas fa-bars fa-2x" onclick="$('#sidebar-collapsed').collapse('toggle');"></i>
                        </li>
                    </ul>
                </div>
                <div id="sidebar-collapsed" class="collapse navbar-collapse">
                    <ul class="navbar-nav text-center d-block d-sm-none">
                        <li class="nav-item"><a href="{{ url("dashboard") }}" class="nav-link"><i class="fa fa-home fa-2x"></i> Dashboard</a></li>
                        <li class="nav-item"><a href="{{ url("projects") }}" class="nav-link"><i class="fas fa-project-diagram fa-2x"></i> Projects</a></li>
                    </ul>
                </div>
            </nav>
            <nav id="sidebar" class="navbar navbar-dark bg-dark mx-0 d-sm-block d-none">
                <ul class="navbar-nav">
                    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard"><a href="{{ url("dashboard") }}" class="nav-link"><i class="fa fa-home fa-2x"></i></a></li>
                    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Projects"><a href="{{ url("projects") }}" class="nav-link"><i class="fas fa-project-diagram fa-2x"></i></a></li>
                </ul>
            </nav>
        </header>
        <div id="relative-content">
            <main class="position-relative">
                @yield ("main")
            </main>
            <footer class="container-fluid w-100 px-0 h-auto bg-dark text-light p-2">
                Footer
            </footer>
        </div>
        <script rel="script" type="text/javascript" src="{{ asset("jquery/jquery-3.4.1.min.js") }}"></script>
        <script rel="script" type="text/javascript" src="{{ asset("popper/popper-1.14.7.min.js") }}"></script>
        <script rel="script" type="text/javascript" src="{{ asset("bootstrap/js/bootstrap-4.3.1.min.js") }}"></script>
        <script rel="script" type="text/javascript" src="{{ asset("js/Default.js") }}"></script>
        @yield ("scripts")
    </body>
</html>
