@extends ("layouts.App")
@section ("head")
    <link rel="stylesheet" type="text/css" href="{{ asset("css/Accounts.css") }}">
@endsection
@section ("title")
    Scrum-app
@endsection
@section ("main")
    <form method="get">
        <div class="searchBar"><input class="form-control" type="text" name="email" placeholder="E-Mail" autocomplete="off"></div>
        <div class="searchBar">
            <select class="form-control" name="authority">
                <option value="%">All Authorities</option>
                @foreach (App\User::ACCOUNT_TYPES as $type)
                    @if ($type != "administrator")
                        <option value="{{ $type }}">{{ $type }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary sharp-corner">Filter</button>
        <a href="{{ url("register") }}"><button type="button" class="btn btn-success sharp-corner">Create account</button></a>
    </form>
    <div>
        <table class="table">
            <thead>
                <tr>
                    <th></th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>E-Mail</th>
                    <th>Verified</th>
                    <th>Authority</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                    <tr class="user" onclick="window.location.href = '{{ url("admin/account?id=".$user->id) }}';">
                        <td><img class="rounded-circle" width="40px" height="40px" src="{{ asset("img/uploads/pfp/".$user->pfp) }}" alt="pfp"></td>
                        <td>{{ $user->firstName }}</td>
                        <td>{{ $user->lastName }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->email_verified_at==null?"No":"Yes" }}</td>
                        <td>{{ $user->authority }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="d-flex flex-row justify-content-center">
        <?php
        function getPageLink($pageNo)
        {
            $search = "";
            foreach($_GET as $key=>$val)
            {
                if($key != "page")
                {
                    $search .= "&$key=$val";
                }
            }
            return url("admin/accounts?page=".$pageNo.$search);
        }
        ?>
        @if ($lastPageNo <= 5)
            @for ($i = 1; $i <= $lastPageNo; $i++)
                <a style="display: inline-block; margin: 0 2px;" href="{{ getPageLink($i) }}">{{ $i }}</a>
            @endfor
        @else
            <a style="display: inline-block; margin: 0 2px;" href="{{ getPageLink(1) }}">1</a>{{ isset($_GET["page"])?($_GET["page"]-2>2?"...":""):"" }}
            @for ($i = 0; $i < 3; $i++)
                <?php $pageNo = min(($lastPageNo-(3-$i)), max(2+$i, (isset($_GET["page"])?$_GET["page"]:1)-(2-$i))); ?>
                <a style="display: inline-block; margin: 0 2px;" href="{{ getPageLink($pageNo) }}">{{ $pageNo }}</a>
            @endfor
            {{ isset($_GET["page"])?($_GET["page"]+4<$lastPageNo?"...":""):"" }}<a style="display: inline-block; margin: 0 2px;" href="{{ getPageLink($lastPageNo) }}">{{ $lastPageNo }}</a>
        @endif
    </div>
@endsection
