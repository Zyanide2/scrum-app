<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();

Route::get("/login", "Auth\LoginController@showLoginForm");
Route::post("/login", "Auth\LoginController@login")->name("login");
Route::get("/register", "Auth\RegisterController@showRegistrationForm");
Route::post("/register", "Auth\RegisterController@register")->name("register");
Route::post("/logout", "Auth\LoginController@logout")->name("logout");

Route::get("/", "IndexController@show");
Route::get("/home", "IndexController@show");
Route::get("/dashboard", "DashboardController@show");
Route::get("/account", "Auth\AccountController@showAccountView");
Route::get("/account/{detail}", "Auth\AccountController@showEditView");
Route::post("/account", "Auth\AccountController@editAccount")->name("account.edit");

Route::get("/projects", "App\ProjectController@showProjectCollectionView");
Route::get("/projects/{uid}", "App\ProjectController@showProjectSingleView");

Route::get("/admin/accounts", "Admin\AccountsController@showAccountCollectionView");
Route::get("/admin/account", "Admin\AccountsController@showAccountSingleView");
Route::get("/admin/account/{detail}", "Admin\AccountsController@showAccountEditView");
